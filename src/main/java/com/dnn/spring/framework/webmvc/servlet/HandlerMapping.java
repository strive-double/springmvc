package com.dnn.spring.framework.webmvc.servlet;

import java.lang.reflect.Method;
import java.util.regex.Pattern;

/**
 * @author dnn
 * @date 2021/11/28 10:06
 */
public class HandlerMapping {
    private Object controller;     //保存方法对应的Controller实例对象
    private Method method;          //保存映射的方法
    private String   URL;        //保存URL

    public HandlerMapping( String URL,Object controller, Method method) {
        this.controller = controller;
        this.method = method;
        this.URL = URL;
    }

    public Object getController() {
        return controller;
    }

    public void setController(Object controller) {
        this.controller = controller;
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }
}
