package com.dnn.spring.framework.webmvc.servlet;

import com.dnn.spring.framework.annontation.Controller;
import com.dnn.spring.framework.annontation.RequestMapping;
import com.dnn.spring.framework.annontation.RequestParam;
import com.dnn.spring.framework.context.ApplicationContext;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.*;



public class DispatcherServlet extends HttpServlet {
    private List<HandlerMapping> handlerMappings = new ArrayList<HandlerMapping>();
    private Map<String,Method> handlerMapping=new HashMap<String,Method>();
    private ApplicationContext context;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setCharacterEncoding("GBK");

        //6、根据URL完成方法的调度
        try {
            doDispatch(req,resp);
        } catch (Exception e) {
            e.printStackTrace();
          resp.getWriter().write("500 Exception Detail " + Arrays.toString(e.getStackTrace()));

        }
    }
    //运行阶段
    private void doDispatch(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        //实现HandlerAdapter功能,参数匹配哪一块
        //调用具体的方法
        String url=req.getRequestURI();//去除主机名的全部路径
        String contextPath=req.getContextPath();//返回工程名部分
        url=("/"+url).replaceAll(contextPath,"").replaceAll("/+","/");
        if(!this.handlerMapping.containsKey(url)){
            resp.getWriter().write("404 Not Found");
            return;
        }
        //形参列表：编译后就能拿到值
        Method method = this.handlerMapping.get(url);
        Map<String,Integer> paramIndexMapping = new HashMap<String, Integer>();

        //提取加了GPRequestParam注解的参数的位置
        Annotation[][] pa = handlerMapping.get(url).getParameterAnnotations();
        for (int i = 0; i < pa.length; i ++){
            for (Annotation a : pa[i]) {
                if(a instanceof RequestParam){
                    String paramName = ((RequestParam) a).value();
                    if(!"".equals(paramName.trim())){
                        paramIndexMapping.put(paramName,i);
                    }
                }
            }
        }

        //提取request和response的位置
        Class<?> [] paramTypes = handlerMapping.get(url).getParameterTypes();
        for (int i = 0; i < paramTypes.length; i++) {
            Class<?> type = paramTypes[i];
            if(type == HttpServletRequest.class || type == HttpServletResponse.class){
                paramIndexMapping.put(type.getName(),i);
            }
        }


        //实参列表：要运行时才能拿到值
        Map<String,String[]> paramsMap = req.getParameterMap();
        //声明实参列表
        Object [] parameValues = new Object[paramTypes.length];
        for (Map.Entry<String,String[]> param : paramsMap.entrySet()) {
            String value = Arrays.toString(paramsMap.get(param.getKey()))
                    .replaceAll("\\[|\\]","")
                    .replaceAll("\\s","");
            if(!paramIndexMapping.containsKey(param.getKey())){continue;}

            int index = paramIndexMapping.get(param.getKey());
            parameValues[index] = caseStringVlaue(value,paramTypes[index]);
        }

        if(paramIndexMapping.containsKey(HttpServletRequest.class.getName())){
            int index = paramIndexMapping.get(HttpServletRequest.class.getName());
            parameValues[index] = req;
        }

        if(paramIndexMapping.containsKey(HttpServletResponse.class.getName())){
            int index = paramIndexMapping.get(HttpServletResponse.class.getName());
            parameValues[index] = resp;
        }



        String beanName = toLowerFirstCase(method.getDeclaringClass().getSimpleName());
        method.invoke(context.getBean(beanName),parameValues);


    }
    private Object caseStringVlaue(String value, Class<?> paramType) {
        if(String.class == paramType){
            return value;
        }
        if(Integer.class == paramType){
            return Integer.valueOf(value);
        }else if(Double.class == paramType){
            return Double.valueOf(value);
        }else {
            if(value != null){
                return value;
            }
            return null;
        }
    }
    private String toLowerFirstCase(String simpleName) {
        char[] chars = simpleName.toCharArray();
        chars[0] += 32;
        return String.valueOf(chars);
    }

    //初始化阶段
    @Override
    public void init(ServletConfig config) throws ServletException {

        context = new ApplicationContext(config.getInitParameter("contextConfigLocation"));

        initStrategies(context);

        System.out.println("DNN Spring framework is init.");

    }
    private void initStrategies(ApplicationContext context) {
        //handlerMapping
        initHandlerMappings(context);


    }

    private void initHandlerMappings(ApplicationContext context) {
        if(context.getBeanDefinitionCount() == 0){ return; }

        String [] beanNames = context.getBeanDefinitionNames();
        for (String beanName : beanNames) {
            Object instance = context.getBean(beanName);
            Class<?> clazz = instance.getClass();
            if(!clazz.isAnnotationPresent(Controller.class)){ continue; }

            String baseUrl = "";
            if(clazz.isAnnotationPresent(RequestMapping.class)){
                RequestMapping requestMapping = clazz.getAnnotation(RequestMapping.class);
                baseUrl = requestMapping.value();
            }

            //默认只获取public方法
            for (Method method : clazz.getMethods()) {
                if(!method.isAnnotationPresent(RequestMapping.class)){continue;}
                RequestMapping requestMapping = method.getAnnotation(RequestMapping.class);
                //   //demo//query
                String url = ("/" + baseUrl + "/" + requestMapping.value()).replaceAll("/+","/");
                handlerMapping.put(url,method);
               // handlerMappings.add(new HandlerMapping(url,instance,method));
                System.out.println("Mapped " + url + "," + method);


            }
        }
    }

}
