package com.dnn.spring.framework.beans.support;

import com.dnn.spring.framework.beans.config.BeanDefinition;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.io.File;

/**
 * 用来读取配置文件的工具类
 * Created by Tom.
 */
public class BeanDefinitionReader {

    private Properties contextConfig = new Properties();

    //需要注册到IoC容器的class
    private List<String> registryBeanClasses = new ArrayList<String>();

    public BeanDefinitionReader(String[] configLocations) {
        //1、读取配置文件
        doLoadConfig(configLocations[0]);

        //2、扫描相关的类
        doScanner(contextConfig.getProperty("scanPackage"));
    }

    public List<BeanDefinition> loadBeanDefinitions() {
        List<BeanDefinition> result = new ArrayList<BeanDefinition>();
        try {
            for (String className : registryBeanClasses) {
                Class<?> beanClass = Class.forName(className);

                //判断自身是不是一个接口
                //如果扫描到的类是com.gupaoedu.vip.demo.service.IQueryService就continue
                //如果扫描到的类是com.gupaoedu.vip.demo.service.impl.ModifyService就继续往下走
                if(beanClass.isInterface()){ continue; }

                //com.gupaoedu.vip.demo.service.impl.ModifyService
                // key = modifyService,value=com.gupaoedu.vip.demo.service.impl.ModifyService
                result.add(doCreateBeanDefinition(toLowerFirstCase(beanClass.getSimpleName()),beanClass.getName()));

                //获得该类实现了哪些接口，并且拿到这些接口的名字
                for (Class<?> i : beanClass.getInterfaces()) {
                    // key = com.gupaoedu.vip.demo.service.IQueryService,value=com.gupaoedu.vip.demo.service.impl.ModifyService
                    result.add(doCreateBeanDefinition(i.getName(),beanClass.getName()));
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return result;
    }

    private BeanDefinition doCreateBeanDefinition(String factoryBeanName, String beanClassName) {
        BeanDefinition beanDefinition = new BeanDefinition();
        beanDefinition.setFactoryBeanName(factoryBeanName);
        beanDefinition.setBeanClassName(beanClassName);
        return beanDefinition;
    }

    private void doLoadConfig(String contextConfigLocation) {
        //直接从ClassPath下去找到Spring的配置文件
        //相当于把application.properties 文件读到了内存中
        InputStream is = this.getClass().getClassLoader().getResourceAsStream(contextConfigLocation.replaceAll("classpath:",""));
        try {
            contextConfig.load(is);
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(null != is){
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private void doScanner(String scanPackage) {
        //去ClassPath下找到相关的.class文件
        //scanPackage包路径，就是对应文件夹
        URL url = this.getClass().getClassLoader().getResource("/" + scanPackage.replaceAll("\\.","/"));
        File classPath = new File(url.getFile());

        for (File file : classPath.listFiles()) {
            if(file.isDirectory()){
                doScanner(scanPackage + "." + file.getName());
            }else {
                //classPath下除了有.class文件，还有 .xml  .properties .yml
                //取反，减少代码嵌套
                //代码嵌套超过三层，就要拉平
                if(!file.getName().endsWith(".class")){ continue; }

                String className = scanPackage + "." + file.getName().replace(".class", "");
//            可以在实例化阶段通过调用Class.forName(className)拿到Class对象
                //从而可以通过反射去创建实例
                registryBeanClasses.add(className);
            }
        }

    }


    //钻牛角尖？
    private String toLowerFirstCase(String simpleName) {
        char [] chars = simpleName.toCharArray();
        //因为大写字符的ASCII码和小写字母的ASCII正好相差32
        //而且大写字母ASCII码要小于小写字母的ASCII码
        chars[0] += 32;
        return String.valueOf(chars);
    }

    public Properties getConfig() {
        return this.contextConfig;
    }
}
