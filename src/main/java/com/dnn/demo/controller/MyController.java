package com.dnn.demo.controller;

import com.dnn.demo.service.IQueryService;
import com.dnn.spring.framework.annontation.Autowired;
import com.dnn.spring.framework.annontation.Controller;
import com.dnn.spring.framework.annontation.RequestMapping;
import com.dnn.spring.framework.annontation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author dnn
 * @date 2021/11/28 9:55
 */
@Controller
@RequestMapping("/web")
public class MyController {
    @Autowired
    IQueryService queryService;


    @RequestMapping("/query.json")
    public void query(HttpServletRequest request, HttpServletResponse response,
                              @RequestParam("name") String name){
        String result = queryService.query(name);
         out(response,result);
    }
    @RequestMapping("/add.json")
    public void add(HttpServletRequest request, HttpServletResponse response,
                      @RequestParam("name")  String name, @RequestParam("id") String id){
        String result = queryService.query(name);
        out(response,result);
    }

    private void out(HttpServletResponse resp,String str){
        try {
            resp.getWriter().write(str);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
